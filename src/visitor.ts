import { Types } from "@graphql-codegen/plugin-helpers";
import {
  ClientSideBasePluginConfig,
  ClientSideBaseVisitor,
  DocumentMode,
  getConfigValue,
  LoadedFragment,
} from "@graphql-codegen/visitor-plugin-common";
import autoBind from "auto-bind";
import { GraphQLSchema, OperationDefinitionNode } from "graphql";
import { pascalCase } from "pascal-case";
import { ReactApolloRawPluginConfig } from "./config";

export interface ReactApolloPluginConfig extends ClientSideBasePluginConfig {
  withComponent: boolean;
  withHOC: boolean;
  withHooks: boolean;
  withMutationFn: boolean;
  withRefetchFn: boolean;
  apolloReactCommonImportFrom: string;
  apolloReactComponentsImportFrom: string;
  apolloReactHocImportFrom: string;
  apolloReactHooksImportFrom: string;
  componentSuffix: string;
  reactApolloVersion: 2 | 3;
  withResultType: boolean;
  withMutationOptionsType: boolean;
  addDocBlocks: boolean;
  omitOperationSuffix: boolean;
}

export class ReactApolloVisitor extends ClientSideBaseVisitor<
  ReactApolloRawPluginConfig,
  ReactApolloPluginConfig
> {
  private _externalImportPrefix: string;
  private imports = new Set<string>();

  constructor(
    schema: GraphQLSchema,
    fragments: LoadedFragment[],
    rawConfig: ReactApolloRawPluginConfig,
    documents: Types.DocumentFile[]
  ) {
    super(schema, fragments, rawConfig, {
      componentSuffix: getConfigValue(rawConfig.componentSuffix, "Component"),
      withHOC: getConfigValue(rawConfig.withHOC, true),
      withComponent: getConfigValue(rawConfig.withComponent, true),
      withHooks: getConfigValue(rawConfig.withHooks, false),
      withMutationFn: getConfigValue(rawConfig.withMutationFn, true),
      withRefetchFn: getConfigValue(rawConfig.withRefetchFn, false),
      apolloReactCommonImportFrom: getConfigValue(
        rawConfig.apolloReactCommonImportFrom,
        rawConfig.reactApolloVersion === 3
          ? "@apollo/client"
          : "@apollo/react-common"
      ),
      apolloReactComponentsImportFrom: getConfigValue(
        rawConfig.apolloReactComponentsImportFrom,
        rawConfig.reactApolloVersion === 3
          ? "@apollo/client"
          : "@apollo/react-components"
      ),
      apolloReactHocImportFrom: getConfigValue(
        rawConfig.apolloReactHocImportFrom,
        rawConfig.reactApolloVersion === 3
          ? "@apollo/client"
          : "@apollo/react-hoc"
      ),
      apolloReactHooksImportFrom: getConfigValue(
        rawConfig.apolloReactHooksImportFrom,
        rawConfig.reactApolloVersion === 3
          ? "@apollo/client"
          : "@apollo/react-hooks"
      ),
      reactApolloVersion: getConfigValue(rawConfig.reactApolloVersion, 2),
      withResultType: getConfigValue(rawConfig.withResultType, true),
      withMutationOptionsType: getConfigValue(
        rawConfig.withMutationOptionsType,
        true
      ),
      addDocBlocks: getConfigValue(rawConfig.addDocBlocks, true),
      omitOperationSuffix: getConfigValue(rawConfig.omitOperationSuffix, false),
    });

    this._externalImportPrefix = this.config.importOperationTypesFrom
      ? `${this.config.importOperationTypesFrom}.`
      : "";
    this._documents = documents;

    autoBind(this);
  }

  private getApolloReactHooksImport(): string {
    return `import * as ApolloReactHooks from '${this.config.apolloReactHooksImportFrom}';`;
  }

  private getDocumentNodeVariable(
    node: OperationDefinitionNode,
    documentVariableName: string
  ): string {
    return this.config.documentMode === DocumentMode.external
      ? `Operations.${node.name?.value}`
      : documentVariableName;
  }

  public getImports(): string[] {
    const baseImports = super.getImports();
    const hasOperations = this._collectedOperations.length > 0;

    if (!hasOperations) {
      return baseImports;
    }

    return [...baseImports, ...Array.from(this.imports)];
  }

  private _buildHooks(
    node: OperationDefinitionNode,
    operationType: string,
    documentVariableName: string,
    operationResultType: string,
    operationVariablesTypes: string
  ): string {
    const suffix = this._getHookSuffix(node.name?.value || "", operationType);
    const operationName: string = this.convertName(node.name?.value || "", {
      suffix,
      useTypesPrefix: false,
    });

    this.imports.add(this.getApolloReactHooksImport());

    const hookFns = [
      `export function use${operationName}(variables${
        operationType === "Mutation" ? "?" : ""
      }: ${operationVariablesTypes},
        baseOptions?: ApolloReactHooks.${operationType}HookOptions<${operationResultType}, ${operationVariablesTypes}>) {
        return ApolloReactHooks.use${operationType}<${operationResultType}, ${operationVariablesTypes}>(${this.getDocumentNodeVariable(
        node,
        documentVariableName
      )}, { variables, ...baseOptions });
      }`,
    ];

    if (operationType === "Query") {
      const lazyOperationName: string = this.convertName(
        node.name?.value || "",
        {
          suffix: pascalCase("LazyQuery"),
          useTypesPrefix: false,
        }
      );
      hookFns.push(
        `export function use${lazyOperationName}(variables?: ${operationVariablesTypes},
          baseOptions?: ApolloReactHooks.LazyQueryHookOptions<${operationResultType}, ${operationVariablesTypes}>) {
          return ApolloReactHooks.useLazyQuery<${operationResultType}, ${operationVariablesTypes}>(${this.getDocumentNodeVariable(
          node,
          documentVariableName
        )}, { variables, ...baseOptions });
        }`
      );
    }

    return [...hookFns].join("\n");
  }

  private _getHookSuffix(name: string, operationType: string) {
    if (this.config.omitOperationSuffix) {
      return "";
    }
    if (!this.config.dedupeOperationSuffix) {
      return pascalCase(operationType);
    }
    if (
      name.includes("Query") ||
      name.includes("Mutation") ||
      name.includes("Subscription")
    ) {
      return "";
    }
    return pascalCase(operationType);
  }

  protected buildOperation(
    node: OperationDefinitionNode,
    documentVariableName: string,
    operationType: string,
    operationResultType: string,
    operationVariablesTypes: string
  ): string {
    operationResultType = this._externalImportPrefix + operationResultType;
    operationVariablesTypes =
      this._externalImportPrefix + operationVariablesTypes;

    const hooks = this.config.withHooks
      ? this._buildHooks(
          node,
          operationType,
          documentVariableName,
          operationResultType,
          operationVariablesTypes
        )
      : null;

    return [hooks].filter((a) => a).join("\n");
  }
}
