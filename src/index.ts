import { PluginFunction, Types } from "@graphql-codegen/plugin-helpers";
import { LoadedFragment } from "@graphql-codegen/visitor-plugin-common";
import {
  concatAST,
  FragmentDefinitionNode,
  GraphQLSchema,
  Kind,
  visit
} from "graphql";
import { ReactApolloRawPluginConfig } from "./config";
import { ReactApolloVisitor } from "./visitor";

const isDefined = <T>(value?: T | null): value is T =>
  value !== undefined && value !== null;

export const plugin: PluginFunction<ReactApolloRawPluginConfig> = (
  schema: GraphQLSchema,
  documents: Types.DocumentFile[],
  config: ReactApolloRawPluginConfig
) => {
  const allAst = concatAST(documents.map(v => v.document).filter(isDefined));

  const allFragments: LoadedFragment[] = [
    ...(allAst.definitions.filter(
      d => d.kind === Kind.FRAGMENT_DEFINITION
    ) as FragmentDefinitionNode[]).map(fragmentDef => ({
      node: fragmentDef,
      name: fragmentDef.name.value,
      onType: fragmentDef.typeCondition.name.value,
      isExternal: false
    })),
    ...(config.externalFragments || [])
  ];

  const visitor = new ReactApolloVisitor(
    schema,
    allFragments,
    config,
    documents
  );
  const visitorResult = visit(allAst, { leave: visitor });

  return {
    prepend: visitor.getImports(),
    content: [
      visitor.fragments,
      ...visitorResult.definitions.filter((t: any) => typeof t === "string")
    ].join("\n")
  };
};

export { ReactApolloVisitor };
