import { PluginFunction } from "@graphql-codegen/plugin-helpers";
import { ReactApolloRawPluginConfig } from "./config";
import { ReactApolloVisitor } from "./visitor";
export declare const plugin: PluginFunction<ReactApolloRawPluginConfig>;
export { ReactApolloVisitor };
