"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const visitor_plugin_common_1 = require("@graphql-codegen/visitor-plugin-common");
const auto_bind_1 = __importDefault(require("auto-bind"));
const pascal_case_1 = require("pascal-case");
class ReactApolloVisitor extends visitor_plugin_common_1.ClientSideBaseVisitor {
    constructor(schema, fragments, rawConfig, documents) {
        super(schema, fragments, rawConfig, {
            componentSuffix: visitor_plugin_common_1.getConfigValue(rawConfig.componentSuffix, "Component"),
            withHOC: visitor_plugin_common_1.getConfigValue(rawConfig.withHOC, true),
            withComponent: visitor_plugin_common_1.getConfigValue(rawConfig.withComponent, true),
            withHooks: visitor_plugin_common_1.getConfigValue(rawConfig.withHooks, false),
            withMutationFn: visitor_plugin_common_1.getConfigValue(rawConfig.withMutationFn, true),
            withRefetchFn: visitor_plugin_common_1.getConfigValue(rawConfig.withRefetchFn, false),
            apolloReactCommonImportFrom: visitor_plugin_common_1.getConfigValue(rawConfig.apolloReactCommonImportFrom, rawConfig.reactApolloVersion === 3
                ? "@apollo/client"
                : "@apollo/react-common"),
            apolloReactComponentsImportFrom: visitor_plugin_common_1.getConfigValue(rawConfig.apolloReactComponentsImportFrom, rawConfig.reactApolloVersion === 3
                ? "@apollo/client"
                : "@apollo/react-components"),
            apolloReactHocImportFrom: visitor_plugin_common_1.getConfigValue(rawConfig.apolloReactHocImportFrom, rawConfig.reactApolloVersion === 3
                ? "@apollo/client"
                : "@apollo/react-hoc"),
            apolloReactHooksImportFrom: visitor_plugin_common_1.getConfigValue(rawConfig.apolloReactHooksImportFrom, rawConfig.reactApolloVersion === 3
                ? "@apollo/client"
                : "@apollo/react-hooks"),
            reactApolloVersion: visitor_plugin_common_1.getConfigValue(rawConfig.reactApolloVersion, 2),
            withResultType: visitor_plugin_common_1.getConfigValue(rawConfig.withResultType, true),
            withMutationOptionsType: visitor_plugin_common_1.getConfigValue(rawConfig.withMutationOptionsType, true),
            addDocBlocks: visitor_plugin_common_1.getConfigValue(rawConfig.addDocBlocks, true),
            omitOperationSuffix: visitor_plugin_common_1.getConfigValue(rawConfig.omitOperationSuffix, false),
        });
        this.imports = new Set();
        this._externalImportPrefix = this.config.importOperationTypesFrom
            ? `${this.config.importOperationTypesFrom}.`
            : "";
        this._documents = documents;
        auto_bind_1.default(this);
    }
    getApolloReactHooksImport() {
        return `import * as ApolloReactHooks from '${this.config.apolloReactHooksImportFrom}';`;
    }
    getDocumentNodeVariable(node, documentVariableName) {
        var _a;
        return this.config.documentMode === visitor_plugin_common_1.DocumentMode.external
            ? `Operations.${(_a = node.name) === null || _a === void 0 ? void 0 : _a.value}`
            : documentVariableName;
    }
    getImports() {
        const baseImports = super.getImports();
        const hasOperations = this._collectedOperations.length > 0;
        if (!hasOperations) {
            return baseImports;
        }
        return [...baseImports, ...Array.from(this.imports)];
    }
    _buildHooks(node, operationType, documentVariableName, operationResultType, operationVariablesTypes) {
        var _a, _b, _c;
        const suffix = this._getHookSuffix(((_a = node.name) === null || _a === void 0 ? void 0 : _a.value) || "", operationType);
        const operationName = this.convertName(((_b = node.name) === null || _b === void 0 ? void 0 : _b.value) || "", {
            suffix,
            useTypesPrefix: false,
        });
        this.imports.add(this.getApolloReactHooksImport());
        const hookFns = [
            `export function use${operationName}(variables${operationType === "Mutation" ? "?" : ""}: ${operationVariablesTypes},
        baseOptions?: ApolloReactHooks.${operationType}HookOptions<${operationResultType}, ${operationVariablesTypes}>) {
        return ApolloReactHooks.use${operationType}<${operationResultType}, ${operationVariablesTypes}>(${this.getDocumentNodeVariable(node, documentVariableName)}, { variables, ...baseOptions });
      }`,
        ];
        if (operationType === "Query") {
            const lazyOperationName = this.convertName(((_c = node.name) === null || _c === void 0 ? void 0 : _c.value) || "", {
                suffix: pascal_case_1.pascalCase("LazyQuery"),
                useTypesPrefix: false,
            });
            hookFns.push(`export function use${lazyOperationName}(variables?: ${operationVariablesTypes},
          baseOptions?: ApolloReactHooks.LazyQueryHookOptions<${operationResultType}, ${operationVariablesTypes}>) {
          return ApolloReactHooks.useLazyQuery<${operationResultType}, ${operationVariablesTypes}>(${this.getDocumentNodeVariable(node, documentVariableName)}, { variables, ...baseOptions });
        }`);
        }
        return [...hookFns].join("\n");
    }
    _getHookSuffix(name, operationType) {
        if (this.config.omitOperationSuffix) {
            return "";
        }
        if (!this.config.dedupeOperationSuffix) {
            return pascal_case_1.pascalCase(operationType);
        }
        if (name.includes("Query") ||
            name.includes("Mutation") ||
            name.includes("Subscription")) {
            return "";
        }
        return pascal_case_1.pascalCase(operationType);
    }
    buildOperation(node, documentVariableName, operationType, operationResultType, operationVariablesTypes) {
        operationResultType = this._externalImportPrefix + operationResultType;
        operationVariablesTypes =
            this._externalImportPrefix + operationVariablesTypes;
        const hooks = this.config.withHooks
            ? this._buildHooks(node, operationType, documentVariableName, operationResultType, operationVariablesTypes)
            : null;
        return [hooks].filter((a) => a).join("\n");
    }
}
exports.ReactApolloVisitor = ReactApolloVisitor;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlzaXRvci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy92aXNpdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQ0Esa0ZBTWdEO0FBQ2hELDBEQUFpQztBQUVqQyw2Q0FBeUM7QUFxQnpDLE1BQWEsa0JBQW1CLFNBQVEsNkNBR3ZDO0lBSUMsWUFDRSxNQUFxQixFQUNyQixTQUEyQixFQUMzQixTQUFxQyxFQUNyQyxTQUErQjtRQUUvQixLQUFLLENBQUMsTUFBTSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUU7WUFDbEMsZUFBZSxFQUFFLHNDQUFjLENBQUMsU0FBUyxDQUFDLGVBQWUsRUFBRSxXQUFXLENBQUM7WUFDdkUsT0FBTyxFQUFFLHNDQUFjLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUM7WUFDaEQsYUFBYSxFQUFFLHNDQUFjLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUM7WUFDNUQsU0FBUyxFQUFFLHNDQUFjLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUM7WUFDckQsY0FBYyxFQUFFLHNDQUFjLENBQUMsU0FBUyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUM7WUFDOUQsYUFBYSxFQUFFLHNDQUFjLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUM7WUFDN0QsMkJBQTJCLEVBQUUsc0NBQWMsQ0FDekMsU0FBUyxDQUFDLDJCQUEyQixFQUNyQyxTQUFTLENBQUMsa0JBQWtCLEtBQUssQ0FBQztnQkFDaEMsQ0FBQyxDQUFDLGdCQUFnQjtnQkFDbEIsQ0FBQyxDQUFDLHNCQUFzQixDQUMzQjtZQUNELCtCQUErQixFQUFFLHNDQUFjLENBQzdDLFNBQVMsQ0FBQywrQkFBK0IsRUFDekMsU0FBUyxDQUFDLGtCQUFrQixLQUFLLENBQUM7Z0JBQ2hDLENBQUMsQ0FBQyxnQkFBZ0I7Z0JBQ2xCLENBQUMsQ0FBQywwQkFBMEIsQ0FDL0I7WUFDRCx3QkFBd0IsRUFBRSxzQ0FBYyxDQUN0QyxTQUFTLENBQUMsd0JBQXdCLEVBQ2xDLFNBQVMsQ0FBQyxrQkFBa0IsS0FBSyxDQUFDO2dCQUNoQyxDQUFDLENBQUMsZ0JBQWdCO2dCQUNsQixDQUFDLENBQUMsbUJBQW1CLENBQ3hCO1lBQ0QsMEJBQTBCLEVBQUUsc0NBQWMsQ0FDeEMsU0FBUyxDQUFDLDBCQUEwQixFQUNwQyxTQUFTLENBQUMsa0JBQWtCLEtBQUssQ0FBQztnQkFDaEMsQ0FBQyxDQUFDLGdCQUFnQjtnQkFDbEIsQ0FBQyxDQUFDLHFCQUFxQixDQUMxQjtZQUNELGtCQUFrQixFQUFFLHNDQUFjLENBQUMsU0FBUyxDQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBQztZQUNuRSxjQUFjLEVBQUUsc0NBQWMsQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQztZQUM5RCx1QkFBdUIsRUFBRSxzQ0FBYyxDQUNyQyxTQUFTLENBQUMsdUJBQXVCLEVBQ2pDLElBQUksQ0FDTDtZQUNELFlBQVksRUFBRSxzQ0FBYyxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDO1lBQzFELG1CQUFtQixFQUFFLHNDQUFjLENBQUMsU0FBUyxDQUFDLG1CQUFtQixFQUFFLEtBQUssQ0FBQztTQUMxRSxDQUFDLENBQUM7UUEvQ0csWUFBTyxHQUFHLElBQUksR0FBRyxFQUFVLENBQUM7UUFpRGxDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLHdCQUF3QjtZQUMvRCxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLHdCQUF3QixHQUFHO1lBQzVDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDUCxJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztRQUU1QixtQkFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pCLENBQUM7SUFFTyx5QkFBeUI7UUFDL0IsT0FBTyxzQ0FBc0MsSUFBSSxDQUFDLE1BQU0sQ0FBQywwQkFBMEIsSUFBSSxDQUFDO0lBQzFGLENBQUM7SUFFTyx1QkFBdUIsQ0FDN0IsSUFBNkIsRUFDN0Isb0JBQTRCOztRQUU1QixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxLQUFLLG9DQUFZLENBQUMsUUFBUTtZQUN2RCxDQUFDLENBQUMsY0FBYyxNQUFBLElBQUksQ0FBQyxJQUFJLDBDQUFFLEtBQUssRUFBRTtZQUNsQyxDQUFDLENBQUMsb0JBQW9CLENBQUM7SUFDM0IsQ0FBQztJQUVNLFVBQVU7UUFDZixNQUFNLFdBQVcsR0FBRyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDdkMsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFFM0QsSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUNsQixPQUFPLFdBQVcsQ0FBQztTQUNwQjtRQUVELE9BQU8sQ0FBQyxHQUFHLFdBQVcsRUFBRSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVPLFdBQVcsQ0FDakIsSUFBNkIsRUFDN0IsYUFBcUIsRUFDckIsb0JBQTRCLEVBQzVCLG1CQUEyQixFQUMzQix1QkFBK0I7O1FBRS9CLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBQSxJQUFJLENBQUMsSUFBSSwwQ0FBRSxLQUFLLEtBQUksRUFBRSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQzFFLE1BQU0sYUFBYSxHQUFXLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBQSxJQUFJLENBQUMsSUFBSSwwQ0FBRSxLQUFLLEtBQUksRUFBRSxFQUFFO1lBQ3JFLE1BQU07WUFDTixjQUFjLEVBQUUsS0FBSztTQUN0QixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMseUJBQXlCLEVBQUUsQ0FBQyxDQUFDO1FBRW5ELE1BQU0sT0FBTyxHQUFHO1lBQ2Qsc0JBQXNCLGFBQWEsYUFDakMsYUFBYSxLQUFLLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUN2QyxLQUFLLHVCQUF1Qjt5Q0FDTyxhQUFhLGVBQWUsbUJBQW1CLEtBQUssdUJBQXVCO3FDQUMvRSxhQUFhLElBQUksbUJBQW1CLEtBQUssdUJBQXVCLEtBQUssSUFBSSxDQUFDLHVCQUF1QixDQUM5SCxJQUFJLEVBQ0osb0JBQW9CLENBQ3JCO1FBQ0M7U0FDSCxDQUFDO1FBRUYsSUFBSSxhQUFhLEtBQUssT0FBTyxFQUFFO1lBQzdCLE1BQU0saUJBQWlCLEdBQVcsSUFBSSxDQUFDLFdBQVcsQ0FDaEQsT0FBQSxJQUFJLENBQUMsSUFBSSwwQ0FBRSxLQUFLLEtBQUksRUFBRSxFQUN0QjtnQkFDRSxNQUFNLEVBQUUsd0JBQVUsQ0FBQyxXQUFXLENBQUM7Z0JBQy9CLGNBQWMsRUFBRSxLQUFLO2FBQ3RCLENBQ0YsQ0FBQztZQUNGLE9BQU8sQ0FBQyxJQUFJLENBQ1Ysc0JBQXNCLGlCQUFpQixnQkFBZ0IsdUJBQXVCO2dFQUN0QixtQkFBbUIsS0FBSyx1QkFBdUI7aURBQzlELG1CQUFtQixLQUFLLHVCQUF1QixLQUFLLElBQUksQ0FBQyx1QkFBdUIsQ0FDdkgsSUFBSSxFQUNKLG9CQUFvQixDQUNyQjtVQUNDLENBQ0gsQ0FBQztTQUNIO1FBRUQsT0FBTyxDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFTyxjQUFjLENBQUMsSUFBWSxFQUFFLGFBQXFCO1FBQ3hELElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRTtZQUNuQyxPQUFPLEVBQUUsQ0FBQztTQUNYO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMscUJBQXFCLEVBQUU7WUFDdEMsT0FBTyx3QkFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQ2xDO1FBQ0QsSUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQztZQUN0QixJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQztZQUN6QixJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxFQUM3QjtZQUNBLE9BQU8sRUFBRSxDQUFDO1NBQ1g7UUFDRCxPQUFPLHdCQUFVLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVTLGNBQWMsQ0FDdEIsSUFBNkIsRUFDN0Isb0JBQTRCLEVBQzVCLGFBQXFCLEVBQ3JCLG1CQUEyQixFQUMzQix1QkFBK0I7UUFFL0IsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixHQUFHLG1CQUFtQixDQUFDO1FBQ3ZFLHVCQUF1QjtZQUNyQixJQUFJLENBQUMscUJBQXFCLEdBQUcsdUJBQXVCLENBQUM7UUFFdkQsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTO1lBQ2pDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUNkLElBQUksRUFDSixhQUFhLEVBQ2Isb0JBQW9CLEVBQ3BCLG1CQUFtQixFQUNuQix1QkFBdUIsQ0FDeEI7WUFDSCxDQUFDLENBQUMsSUFBSSxDQUFDO1FBRVQsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzdDLENBQUM7Q0FDRjtBQS9LRCxnREErS0MifQ==