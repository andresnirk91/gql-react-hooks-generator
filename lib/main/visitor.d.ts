import { Types } from "@graphql-codegen/plugin-helpers";
import { ClientSideBasePluginConfig, ClientSideBaseVisitor, LoadedFragment } from "@graphql-codegen/visitor-plugin-common";
import { GraphQLSchema, OperationDefinitionNode } from "graphql";
import { ReactApolloRawPluginConfig } from "./config";
export interface ReactApolloPluginConfig extends ClientSideBasePluginConfig {
    withComponent: boolean;
    withHOC: boolean;
    withHooks: boolean;
    withMutationFn: boolean;
    withRefetchFn: boolean;
    apolloReactCommonImportFrom: string;
    apolloReactComponentsImportFrom: string;
    apolloReactHocImportFrom: string;
    apolloReactHooksImportFrom: string;
    componentSuffix: string;
    reactApolloVersion: 2 | 3;
    withResultType: boolean;
    withMutationOptionsType: boolean;
    addDocBlocks: boolean;
    omitOperationSuffix: boolean;
}
export declare class ReactApolloVisitor extends ClientSideBaseVisitor<ReactApolloRawPluginConfig, ReactApolloPluginConfig> {
    private _externalImportPrefix;
    private imports;
    constructor(schema: GraphQLSchema, fragments: LoadedFragment[], rawConfig: ReactApolloRawPluginConfig, documents: Types.DocumentFile[]);
    private getApolloReactHooksImport;
    private getDocumentNodeVariable;
    getImports(): string[];
    private _buildHooks;
    private _getHookSuffix;
    protected buildOperation(node: OperationDefinitionNode, documentVariableName: string, operationType: string, operationResultType: string, operationVariablesTypes: string): string;
}
