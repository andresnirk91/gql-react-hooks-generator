"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const visitor_1 = require("./visitor");
exports.ReactApolloVisitor = visitor_1.ReactApolloVisitor;
const isDefined = (value) => value !== undefined && value !== null;
exports.plugin = (schema, documents, config) => {
    const allAst = graphql_1.concatAST(documents.map(v => v.document).filter(isDefined));
    const allFragments = [
        ...allAst.definitions.filter(d => d.kind === graphql_1.Kind.FRAGMENT_DEFINITION).map(fragmentDef => ({
            node: fragmentDef,
            name: fragmentDef.name.value,
            onType: fragmentDef.typeCondition.name.value,
            isExternal: false
        })),
        ...(config.externalFragments || [])
    ];
    const visitor = new visitor_1.ReactApolloVisitor(schema, allFragments, config, documents);
    const visitorResult = graphql_1.visit(allAst, { leave: visitor });
    return {
        prepend: visitor.getImports(),
        content: [
            visitor.fragments,
            ...visitorResult.definitions.filter((t) => typeof t === "string")
        ].join("\n")
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQSxxQ0FNaUI7QUFFakIsdUNBQStDO0FBeUN0Qyw2QkF6Q0EsNEJBQWtCLENBeUNBO0FBdkMzQixNQUFNLFNBQVMsR0FBRyxDQUFJLEtBQWdCLEVBQWMsRUFBRSxDQUNwRCxLQUFLLEtBQUssU0FBUyxJQUFJLEtBQUssS0FBSyxJQUFJLENBQUM7QUFFM0IsUUFBQSxNQUFNLEdBQStDLENBQ2hFLE1BQXFCLEVBQ3JCLFNBQStCLEVBQy9CLE1BQWtDLEVBQ2xDLEVBQUU7SUFDRixNQUFNLE1BQU0sR0FBRyxtQkFBUyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFFM0UsTUFBTSxZQUFZLEdBQXFCO1FBQ3JDLEdBQUksTUFBTSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQzNCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxjQUFJLENBQUMsbUJBQW1CLENBQ1osQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2pELElBQUksRUFBRSxXQUFXO1lBQ2pCLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUs7WUFDNUIsTUFBTSxFQUFFLFdBQVcsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUs7WUFDNUMsVUFBVSxFQUFFLEtBQUs7U0FDbEIsQ0FBQyxDQUFDO1FBQ0gsR0FBRyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsSUFBSSxFQUFFLENBQUM7S0FDcEMsQ0FBQztJQUVGLE1BQU0sT0FBTyxHQUFHLElBQUksNEJBQWtCLENBQ3BDLE1BQU0sRUFDTixZQUFZLEVBQ1osTUFBTSxFQUNOLFNBQVMsQ0FDVixDQUFDO0lBQ0YsTUFBTSxhQUFhLEdBQUcsZUFBSyxDQUFDLE1BQU0sRUFBRSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBRXhELE9BQU87UUFDTCxPQUFPLEVBQUUsT0FBTyxDQUFDLFVBQVUsRUFBRTtRQUM3QixPQUFPLEVBQUU7WUFDUCxPQUFPLENBQUMsU0FBUztZQUNqQixHQUFHLGFBQWEsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBTSxFQUFFLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxRQUFRLENBQUM7U0FDdkUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0tBQ2IsQ0FBQztBQUNKLENBQUMsQ0FBQyJ9